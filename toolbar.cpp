#include "toolbar.h"

Toolbar::Toolbar()
{
    b1 = new QAction("Add Video",this);
    b2 = new QAction("Rename Video",this);
    //b3 = new QAction("Edit Tags",this);
    b4 = new QAction("Search",this);

    connect(b1, SIGNAL(triggered()), this, SIGNAL(addVid()));
    //connect(b2, SIGNAL(triggered()), this, SIGNAL(renameVid()));
    //connect(b3, SIGNAL(triggered()), this, SIGNAL(editTags()));
    connect(b4, SIGNAL(triggered()), this, SIGNAL(lib()));

    this->addAction(b1);
    this->addAction(b2);
    //this->addAction(b3);
    this->addAction(b4);

//    setStyleSheet("background-color: lightgray;");
}
