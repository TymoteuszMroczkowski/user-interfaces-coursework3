#ifndef VIDEO_PLAYER_WIDGET_H
#define VIDEO_PLAYER_WIDGET_H
#include "library_widget.h"
#include "controls_widget.h"
#include "the_button.h"
#include "the_player.h"
#include "toolbar.h"
#include "add_video_widget.h"
#include <QVideoWidget>
#include <QtWidgets>


class video_player_widget: public QWidget
{
    Q_OBJECT
public:
    video_player_widget(std::vector<TheButtonInfo> &videos, int j);

private:
    void createWidgets(std::vector<TheButtonInfo> &videos, int j);
    void setupSlots();

    LibraryWidget* library;
    ControlsWidget* controls;
    QWidget* rightWidget;
    Toolbar* bar;
    addVideoWidget* addVid;
    renameVideoWidget* renameVid;

private slots:
    void showAddVid();
    void showRenameVid();
    void showLib();
};

#endif // VIDEO_PLAYER_WIDGET_H
