#include "controls_widget.h"
#include <QtWidgets>
#include <iostream>

ControlsWidget::ControlsWidget(ThePlayer* player){
    media = player;
    createWidgets();
    setupSlots();
    addPlayer(media);

    // it is a bit hacky / to change if widget is clipping
    setMaximumHeight(size().height()/5*2);

    // set initial player state to true
    playstate = true;
}

void ControlsWidget::setPlaystate(bool playState){
    this->playstate=playState;
}

void ControlsWidget::createWidgets(){

    QGridLayout* grid = new QGridLayout;

    slider = new QSlider(Qt::Horizontal);

    // Label to hold the title of the video
    title = new QLabel("Title of current video");
    title->setStyleSheet("font-size: 30px;\
                         background-color: darkgray;");
    title->setMaximumHeight(40);

    // Label to hold the tags
    tags = new QTextEdit(this);
    tags->setPlaceholderText("Tags: #Sport #Place #Weather etc.");

    // Create the buttons and set their icons
    favButton = new QPushButton();
//    favButton->setIcon(QIcon("../videos/resources/star.png"));
    favButton->setIcon(QIcon(":/resources/star.png"));
    setupButton(favButton);

    volButton = new QPushButton();
    volButton->setIcon(QIcon(":/resources/volume.png"));
    setupButton(volButton);

    prevButton = new QPushButton();
    prevButton->setIcon(QIcon(":/resources/prev.png"));
    setupButton(prevButton);

    pauseButton = new QPushButton();
    pauseButton->setIcon(QIcon(":/resources/pause.png"));
    setupButton(pauseButton);

    nextButton = new QPushButton();
    nextButton->setIcon(QIcon(":/resources/next.png"));
    setupButton(nextButton);

    // Add widgets to the grid
    grid->addWidget(slider,0,0,1,20);
    grid->addWidget(title,1,0,1,12);
    grid->addWidget(tags,1,12,-1,8);
    grid->addWidget(favButton,2,0,1,1);
    grid->addWidget(volButton,2,1,1,1);
    grid->addWidget(prevButton,2,2,1,1);
    grid->addWidget(pauseButton,2,3,1,1);
    grid->addWidget(nextButton,2,4,1,1);

    setLayout(grid);
    setStyleSheet("background-color: lightgray;");
}

void ControlsWidget::setupSlots(){
    connect(pauseButton,SIGNAL(clicked()),this,SLOT(playStateChange()));
    connect(volButton, SIGNAL(clicked()), media, SLOT(mute()));
    connect(favButton, SIGNAL(clicked()), this, SLOT(changeStarredState()));
    connect(nextButton, SIGNAL(clicked()), this, SLOT(nextVideo()));
    connect(prevButton, SIGNAL(clicked()), this, SLOT(prevVideo()));
    connect(media,SIGNAL(positionChanged(qint64)),this,SLOT(setSlider(qint64)));
    connect(slider,SIGNAL(sliderReleased()),this,SLOT(convSlider()));
    connect(this,SIGNAL(sliderChanged(qint64)),media,SLOT(setPosition(qint64)));
}

void ControlsWidget::setupButton(QPushButton *button){
//    button->setMaximumSize(QSize(160,160));
//    button->setStyleSheet("border-radius: 5px;");
    button->setMinimumSize(QSize(60,60));
    button->setIconSize(QSize(50,50));
}

void ControlsWidget::playStateTrue(){
    playstate = true;
    changePlayState();
}

void ControlsWidget::playStateChange(){
    playstate = !playstate;
    changePlayState();
}

void ControlsWidget::changePlayState(){
    if(playstate){
        pauseButton->setIcon(QIcon(":/resources/pause.png"));
    }
    else{
        pauseButton->setIcon(QIcon(":/resources/play.png"));
    }
    pauseButton->setIconSize(QSize(50,50));
    media->playStateChanged(playstate);
}

void ControlsWidget::addPlayer(QMediaPlayer *media){
    connect(this,SIGNAL(pause()),media,SLOT(pause()));
    connect(this,SIGNAL(play()),media,SLOT(play()));
    connect(media,SIGNAL(videoChanged()),this,SLOT(playStateTrue()));
}

void ControlsWidget::jumpTo(TheButtonInfo* button)
{
    title->setText(button->name);
}

void ControlsWidget::changeStarredState(){
    if(starstate == false){
        favButton->setIcon(QIcon(":/resources/star2.png"));
        starstate = true;
    }
    else{
        favButton->setIcon(QIcon(":/resources/star.png"));
        starstate = false;
    }
}

void ControlsWidget::nextVideo(){
    int nextvideo = media->getVideoIndex() + 1;
    if(nextvideo < 6)
        media->jumpTo(buttons.at(nextvideo)->info);
    else
        media->jumpTo(buttons.at(0)->info);
}

void ControlsWidget::prevVideo(){
    int nextvideo = media->getVideoIndex() - 1;
    if(nextvideo > -1)
        media->jumpTo(buttons.at(nextvideo)->info);
    else
        media->jumpTo(buttons.at(5)->info);
}
void ControlsWidget::setSlider(qint64 mili){
    slider->setMaximum(media->duration());
    slider->setMinimum(0);

    if(!slider->isSliderDown())
        slider->setValue(mili);
}

void ControlsWidget::convSlider(){
    qint64 pos = static_cast<qint64>(slider->value());
    emit sliderChanged(pos);
}
