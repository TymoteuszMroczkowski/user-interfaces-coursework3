Below is a step-by-step guide on how to run the code, the same instructions are also on the process document:

Instructions on how to run our project of Tomeo are somewhat similar to the instructions provided to us at the beginning of the coursework. Despite this, below is a step-by-step guide on how to download and run the project:
1. Open your terminal and in the console enter “git clone https://gitlab.com/TymoteuszMroczkowski/user-interfaces-coursework3.git”. This will clone the repository and download all the files necessary for the project. If this doesn't work, access the link and locate the download button to download a zip file.

2. Locate the file and, if it’s in zip format, extract the contents. In Qt, on the “file” tab, click on “Open file or project” and locate the .pro file in the repository. Once found, this should load all the classes and header files necessary to run the project.

3. Run the project. When you do, a popup will appear to open up a OneDrive link. Click it and download the contents of the link. Extract the videos from the link, and set the path of the videos as the first command line argument in quotes (“”)

4. Click the run button, and the application should open and run.
