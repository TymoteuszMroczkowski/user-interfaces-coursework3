#ifndef MAP_WIDGET_H
#define MAP_WIDGET_H
#include "library_widget.h"
#include "controls_widget.h"
#include "the_button.h"
#include "the_player.h"
#include "toolbar.h"
#include "add_video_widget.h"
#include <QVideoWidget>
#include <QtWidgets>


class map_widget: public QWidget
{
    Q_OBJECT
public:
    map_widget();
    void createWidget();
};

#endif // MAP_WIDGET_H
