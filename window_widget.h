#ifndef WINDOWWIDGET_H
#define WINDOWWIDGET_H

#include "library_widget.h"
#include "controls_widget.h"
#include "the_button.h"
#include "the_player.h"
#include "toolbar.h"
#include "add_video_widget.h"
#include "video_player_widget.h"
#include "map_layout.h"
#include <QVideoWidget>
#include <QtWidgets>

class WindowWidget: public QWidget{
    Q_OBJECT
public:
    WindowWidget(std::vector<TheButtonInfo> &videos);

private:
    void createWidgets(std::vector<TheButtonInfo> &videos);
    void createMapWidgets(std::vector<TheButtonInfo> &videos);
    void setupSlots();

    LibraryWidget* library;
    ControlsWidget* controls;
    QWidget* rightWidget;
    Toolbar* bar;
    addVideoWidget* addVid;
    renameVideoWidget* renameVid;
    video_player_widget* player;
    std::vector<TheButtonInfo> &videos;
    int currentButton;
    bool videoOpen = false;

public slots:
    void openPlayer();

private slots:
    void showAddVid();
    void showRenameVid();
    void showLib();
};

#endif // WINDOWWIDGET_H
