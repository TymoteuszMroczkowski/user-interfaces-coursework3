#include "map_layout.h"

int map_layout::count() const {
    return list_.size();
}

QLayoutItem *map_layout::itemAt(int idx) const {
    return list_.value(idx);
}

QLayoutItem *map_layout::takeAt(int idx) {
    return idx >= 0 && idx < list_.size() ? list_.takeAt(idx) : 0;
}

void map_layout::addItem(QLayoutItem *item) {
    list_.append(item);
}

QSize map_layout::sizeHint() const {
    return minimumSize();
}

QSize map_layout::minimumSize() const {
    return QSize(320,320);
}

