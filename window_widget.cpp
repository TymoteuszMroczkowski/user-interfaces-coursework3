#include "window_widget.h"
#include "add_video_widget.h"
#include "library_widget.h"
#include <iostream>
#include <cstdlib>
#include <QPixmap>

using namespace std;

WindowWidget::WindowWidget(std::vector<TheButtonInfo> &videos) : videos(videos){
    createMapWidgets(videos);
    setupSlots();
}

void WindowWidget::createMapWidgets(std::vector<TheButtonInfo> &videos){
    // a list of the buttons
    std::vector<TheButton*> buttons;

    bar = new Toolbar();

    ThePlayer *player = new ThePlayer;
    ControlsWidget* controls = new ControlsWidget(player);
    library = new LibraryWidget(videos, player, controls, 0);

    addVid = new addVideoWidget();
    renameVid = new renameVideoWidget();

    //creates widget and layout for the main screen
    QWidget *mapWidget = new QWidget();;
    map_layout *mapLayout = new map_layout();
    QVBoxLayout *windowLayout = new QVBoxLayout();

    int j = 0;

    //iterate over all the videos adding a new button for each one
    for(int i=2; i<6+2;i++) {
        TheButton *button = new TheButton(mapWidget);
        mapLayout->addWidget(button);
        buttons.push_back(button);
        mapLayout->addWidget(button);
        button->init( &videos.at(i-2) );
        button->info->setVectorIndex(j);
        //places button in a relavent position on the window
        if(i == 2)
            button->setGeometry(530, 250, 50, 50);
        else if(i == 3)
            button->setGeometry(980, 570, 50, 50);
        else if(i == 4)
            button->setGeometry(590, 270, 50, 50);
        else if(i == 5)
            button->setGeometry(900, 250, 50, 50);
        else if(i == 6)
            button->setGeometry(825, 380, 50, 50);
        else if(i == 7)
            button->setGeometry(350, 520, 50, 50);
        j++;
        //setup slots for each button
        button->connect(button, SIGNAL(clicked()), this, SLOT(openPlayer()));
    }
    //makes background of the main screen a map and adds everything to the window layout
    mapWidget->setStyleSheet("background-image:url(:/resources/map5.png); background-position: center; ");
    mapWidget->setLayout(mapLayout);
    windowLayout->addWidget(bar);
    windowLayout->addWidget(mapWidget);
    setLayout(windowLayout);
}

void WindowWidget::showAddVid(){
    addVid->show();
}

void WindowWidget::showRenameVid(){
    renameVid->show();
}


void WindowWidget::showLib(){
    library->show();
}

void WindowWidget::openPlayer(){
    //this method is only opened when a button is clicked on the map screen
    if(videoOpen == false){
        videoOpen = true;
        //iterates through the vector of the buttons to check which has being clicked
        for(int i = 0; i<videos.size(); i++){
            if(videos[i].isClicked == true){
                currentButton = videos[i].getVectorIndex();
            }
        }
        //shows the pressed button in a new window
        player = new video_player_widget(videos, currentButton);
        player->show();
        videos[currentButton].isClicked = false;
        videoOpen = false;
    }
}

void WindowWidget::setupSlots(){
    // Connect toolbar to widgets
    connect(bar, SIGNAL(addVid()), this, SLOT(showAddVid()));
    connect(bar, SIGNAL(renameVid()), this, SLOT(showRenameVid()));
    connect(bar, SIGNAL(lib()), this, SLOT(showLib()));
}

