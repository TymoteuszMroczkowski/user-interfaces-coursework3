#include "add_video_widget.h"
#include <QtWidgets>

// Widget to add a new video
addVideoWidget::addVideoWidget(){
    // Create Widgets and setup the addVideoWidget
    createWidgets();
    setupSlots();
    setWindowTitle("Add Video");
    setMinimumSize(QSize(320,240));
    setMaximumHeight(sizeHint().height());
}

void addVideoWidget::createWidgets(){
    // Create and setup the widgets and layouts
    QFormLayout* form = new QFormLayout();
    form->setSpacing(10);

    name = new QLineEdit(this);
    name->setPlaceholderText("Title");

    tags = new QTextEdit(this);
    tags->setPlaceholderText("#Sport #Place etc.");

    link = new QLineEdit(this);
    link->setPlaceholderText("Path/To/Video");

    addButton = new QPushButton("&Add",this);
    addButton->setMaximumWidth(80);

    form->addRow("&Name:",name);
    form->addRow("&Tags:",tags);
    form->addRow("&Path:",link);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->insertWidget(1,addButton,1,Qt::AlignRight);

    form->addRow(layout);

    setLayout(form);
}

void addVideoWidget::setupSlots(){
    connect(addButton,SIGNAL(clicked()),this,SLOT(close()));
}

// Widget to rename currently playing video
renameVideoWidget::renameVideoWidget(){
    createWidgets();
    setupSlots();
    setWindowTitle("Rename Video");
    setMinimumSize(QSize(320,sizeHint().height()));
    setMaximumHeight(sizeHint().height());
}

void renameVideoWidget::createWidgets(){
    QFormLayout* form = new QFormLayout();
    form->setSpacing(10);

    name = new QLineEdit(this);
    name->setPlaceholderText("New Title");

    renameButton = new QPushButton("&Rename",this);
    renameButton->setMaximumWidth(80);

    form->addRow("&New Name:",name);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->insertWidget(1,renameButton,1,Qt::AlignRight);

    form->addRow(layout);

    setLayout(form);
}

void renameVideoWidget::setupSlots(){
    connect(renameButton,SIGNAL(clicked()),this,SLOT(close()));
}
