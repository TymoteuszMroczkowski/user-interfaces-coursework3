#include "video_player_widget.h"

video_player_widget::video_player_widget(std::vector<TheButtonInfo> &videos, int currentVideoVector)
{
    createWidgets(videos, currentVideoVector);
    setupSlots();
    setWindowTitle("Tomeo Player");
    setMinimumSize(QSize(800,600));
}

void video_player_widget::createWidgets(std::vector<TheButtonInfo> &videos, int currentVideoVector){
    // the widget that will show the video
    QVideoWidget *videoWidget = new QVideoWidget;
    // the QMediaPlayer which controls the playback
    ThePlayer *player = new ThePlayer;
    //Adds the video player to a horizontal layout for when its merged with bottomButtonWidget
    QVBoxLayout *videoLayout = new QVBoxLayout();
    player->setVideoOutput(videoWidget);
    videoLayout->addWidget(videoWidget);

    //add the horizontal buttons and player to a vertical layout
    ControlsWidget* controls = new ControlsWidget(player);
    this->controls=controls;
    QWidget *rightWidget = new QWidget();
    QVBoxLayout *rightLayout = new QVBoxLayout();
    rightLayout->addWidget(videoWidget);
    rightLayout->addWidget(controls);
    rightWidget->setLayout(rightLayout);
    this->rightWidget=rightWidget;

    // adds the library widget to the layout
    LibraryWidget *leftWidget = new LibraryWidget(videos, player, controls, currentVideoVector);
    this->library=leftWidget;

    //NOT BEING USED BUT IF DELETED CAUSES CRASH
    QWidget *bottomButtonWidget = new QWidget();
    // a list of the buttons
    std::vector<TheButton*> buttons;
    // the buttons are arranged horizontally
    QHBoxLayout *Button1layout = new QHBoxLayout();
    bottomButtonWidget->setLayout(Button1layout);

   //NOT BEING USED BUT IF DELETED CAUSES CRASH
   for ( int i = 0; i < 6; i++ ) {
        TheButton *button = new TheButton(bottomButtonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo*))); // when clicked, tell the player to play.
        buttons.push_back(button);
        Button1layout->addWidget(button);
        button->setControlWidget(controls);
        button->init(&videos.at(i));
    }

    // tell the player what buttons and videos are available
    player->setContent(&buttons, & videos, currentVideoVector);

    controls->setButtons(leftWidget->getButtons());

    // create the main window
    QWidget window;

    //creates the toolbar and adds the controls and library to one layout
    bar = new Toolbar();

    QVBoxLayout *top = new QVBoxLayout();

    addVid = new addVideoWidget();
    renameVid = new renameVideoWidget();

    // add the tool bar, library and controls to the layout
    top->addWidget(bar);
    top->addWidget(rightWidget);
    setLayout(top);
}

void video_player_widget::setupSlots(){
    // Connects toolbar to widgets
    connect(bar, SIGNAL(addVid()), this, SLOT(showAddVid()));
    connect(bar, SIGNAL(renameVid()), this, SLOT(showRenameVid()));
    connect(bar, SIGNAL(lib()), this, SLOT(showLib()));
}

void video_player_widget::showAddVid(){
    addVid->show();
}

void video_player_widget::showRenameVid(){
    renameVid->show();
}

void video_player_widget::showLib(){
    library->show();
}
