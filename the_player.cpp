//
// Created by twak on 11/11/2019.
//

#include "the_player.h"

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i, int currentVideoVector) {
    buttons = b;
    infos = i;
    jumpTo(buttons -> at(currentVideoVector) -> info);
    currentButton = buttons -> at(currentVideoVector) -> info;
    connect(this,SIGNAL(stateChanged(QMediaPlayer::State)),this,SLOT(replay(QMediaPlayer::State)));
}

// change the image and video for one button every one second
void ThePlayer::shuffle() {
    TheButtonInfo* i = & infos -> at (rand() % infos->size() );
//        setMedia(*i->url);
    buttons -> at( updateCount++ % buttons->size() ) -> init( i );
}

void ThePlayer::playStateChanged (bool playstate) {
    //checks if the previous button is paused or playing and puts the new video in that state
    playState = playstate;
    counter = 1;
    if(playstate == true){
        play(); // starting playing again...
    }
    else{
        pause();
    }
}

void ThePlayer::jumpTo (TheButtonInfo* button) {
    currentButton = button;
    setMedia( * button -> url);
    if(counter > 0)
        this->playStateChanged(playState);
    else
        play();
}

void ThePlayer::replay(QMediaPlayer::State state){
    if(state == QMediaPlayer::StoppedState)
        play();
}

void ThePlayer::mute(){
    if(muted == true){
        setVolume(70);
        muted = false;
    }
    else{
        setVolume(0);
        muted = true;
    }
}

int ThePlayer::getVideoIndex(){
    return currentButton->getVectorIndex();
}
