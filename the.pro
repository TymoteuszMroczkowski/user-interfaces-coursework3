QT += core gui widgets multimedia multimediawidgets

CONFIG += c++11


# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    add_video_widget.cpp \
    controls_widget.cpp \
    library_widget.cpp \
    map_layout.cpp \
        the_button.cpp \
        the_player.cpp \
        tomeo.cpp \
    toolbar.cpp \
    video_player_widget.cpp \
    window_widget.cpp

HEADERS += \
    add_video_widget.h \
    controls_widget.h \
    library_widget.h \
    map_layout.h \
    the_button.h \
    the_player.h \
    toolbar.h \
    video_player_widget.h \
    window_widget.h

RESOURCES     = resources.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

