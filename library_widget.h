#ifndef LIBRARY_WIDGET_H
#define LIBRARY_WIDGET_H
#include <QWidget>
#include <QLayout>
#include <QLineEdit>
#include "controls_widget.h"
#define MARGIN 10
//Main coder: Tymoteusz


class LibraryWidget : public QWidget {
Q_OBJECT
public:
    LibraryWidget();
    LibraryWidget(std::vector<TheButtonInfo> &buttonInfo,ThePlayer* player, ControlsWidget* controls, int j);
    //I use theese to copy the player and info about vids into the class (tried doing it with pointers to avoid coppying vectors but failed)

    void setWindow(QWidget *window);

    // Resizes widget accordingly to the window
    void re();
    std::vector<TheButton*> getButtons(){return buttons;}
private:
    //variables used by functions
    int nOfVids;
    ControlsWidget *controlWidget;
    QWidget *window;
    QWidget *buttonWidget; //stores the wiget containing all TheButtons
    ThePlayer *player; //Stores a pointer to ThePlayer

    std::vector<TheButtonInfo>buttonInfo; //copy of the vector of info for Buttons TODO: make it a pointer to avoid copying
    std::vector<TheButton*> buttons; //vector of all the buttons
    int cols; //number of columns
    void setCols();
};

#endif // LIBRARY_WIDGET_H
