#ifndef ADDVIDEOWIDGET_H
#define ADDVIDEOWIDGET_H

#include <QtWidgets>

// Widget to add a new video to the library
class addVideoWidget: public QWidget{
public:
    addVideoWidget();
private:
    void createWidgets();
    void setupSlots();

    QPushButton* addButton;
    QLineEdit* name;
    QTextEdit* tags;
    QLineEdit* link;
};

// Widget to rename currently playing video
class renameVideoWidget: public QWidget{
public:
    renameVideoWidget();
private:
    void createWidgets();
    void setupSlots();

    QPushButton* renameButton;
    QLineEdit* name;
};

#endif // ADDVIDEOWIDGET_H
