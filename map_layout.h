#ifndef MAP_LAYOUT_H
#define MAP_LAYOUT_H
#include "library_widget.h"
#include "controls_widget.h"
#include "the_button.h"
#include "the_player.h"
#include "toolbar.h"
#include "add_video_widget.h"
#include <QVideoWidget>
#include <QtWidgets>


class map_layout : public QLayout
{
public:
    map_layout(): QLayout() {}

    void addItem(QLayoutItem *item);
    QSize sizeHint() const;
    QSize minimumSize() const;
    int count() const;
    QLayoutItem *itemAt(int) const;
    QLayoutItem *takeAt(int);

private:
    QList<QLayoutItem *> list_;
};

#endif // MAP_LAYOUT_H
