//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_PLAYER_H
#define CW2_THE_PLAYER_H


#include <QApplication>
#include <QMediaPlayer>
#include "the_button.h"
#include <vector>
#include <QTimer>

class ThePlayer : public QMediaPlayer {

Q_OBJECT

private:
    std::vector<TheButtonInfo>* infos;
    std::vector<TheButton*>* buttons;
    QTimer* mTimer;  
    bool muted = true;
    long updateCount = 0;

public:
    ThePlayer() : QMediaPlayer(NULL) {
        setVolume(0); // be slightly less annoying
//        connect (this, SIGNAL (stateChanged(QMediaPlayer::State)), this, SLOT (playStateChanged(QMediaPlayer::State)) );

        mTimer = new QTimer(NULL);
        mTimer->setInterval(1000); // 1000ms is one second between ...
        mTimer->start();
        //connect( mTimer, SIGNAL (timeout()), SLOT ( shuffle() ) ); // ...running shuffle method
        setNotifyInterval(10);
    }

    // all buttons have been setup, store pointers here
    void setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i, int j);
    int getVideoIndex();

    bool playState;

    TheButtonInfo *currentButton;

    bool counter = 0;

private slots:

    // change the image and video for one button every one second
    void shuffle();
    void replay(QMediaPlayer::State state);



public slots:
    void playStateChanged (bool playstate);
    // start playing this ButtonInfo
    void jumpTo (TheButtonInfo* button);
    void mute();

signals:
    void videoChanged();
};

#endif //CW2_THE_PLAYER_H
