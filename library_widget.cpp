#include "library_widget.h"

LibraryWidget::LibraryWidget(std::vector<TheButtonInfo> &buttonInfo,ThePlayer* player, ControlsWidget* controls, int currentVideoVector) {
    //assign passed variables to in-class variables
    this->player = player;
    this->buttonInfo = buttonInfo;
    this->controlWidget = controls;

    //create the outermost layut
    QVBoxLayout *container = new QVBoxLayout();
    container->setSpacing(0);
    this->setLayout(container);

    //initiate the vector of buttons
    buttons = std::vector<TheButton*>();
    nOfVids = buttonInfo.size();

    //add the searchBar
    QHBoxLayout *searchBar = new QHBoxLayout();
    searchBar->setSpacing(MARGIN);
    container->addLayout(searchBar);
    QLineEdit *searchField = new QLineEdit();
    searchBar->addWidget(searchField);
    QPushButton *searchButton = new QPushButton("Search");
    searchBar->addWidget(searchButton);

    //set variables & initiate the inner widget
    setCols();
    buttonWidget = new QWidget();

    //set a grid layout for the wiget
    QGridLayout *library = new QGridLayout();
    library->setSpacing(MARGIN);
    buttonWidget->setLayout(library);

    //creates widgets for the starred button and filter putton
    QPushButton *starredButton = new QPushButton("Starred");
    QPushButton *filterButton = new QPushButton("Filter");
    library->addWidget(starredButton, 0/cols, 1%cols);
    library->addWidget(filterButton, 1/cols, 2%cols);
    int k = 0;

    //iterate over all the videos adding a new button for each one
    for(int i=2; i<nOfVids+2;i++) {
        TheButton *button = new TheButton(buttonWidget);

        //i/col is the number of row (i+1)%col is the number of column for the i'th vid
        library->addWidget(button,i/cols,(i+1)%cols);

        //connectig SIGNALS and SLOTS coppied over from main, IDK if works, gotta integrarte to test
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo*)), player, SLOT (jumpTo(TheButtonInfo*)));
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo*)), controlWidget, SLOT(jumpTo(TheButtonInfo*)));

        buttons.push_back(button);
        library->addWidget(button);
        button->init( &buttonInfo.at(i-2) );
        button->setControlWidget(controlWidget);
        button->info->setVectorIndex(k);
        k++;
    }
    //connect to player
    player->setContent(&buttons,&buttonInfo, currentVideoVector);
    buttons.at(0)->emit jumpTo(&buttonInfo.at(0));
    //add the inner wiget to the main one and align all to the top
    container->addWidget(buttonWidget);
    container->addStretch();
}

//just sets it to 2
void LibraryWidget::setCols() {
    cols=2;
}

void LibraryWidget::setWindow(QWidget *window){
    this->window=window;
}

// Resizes widget accordingly to the window
void LibraryWidget::re(){
    setMinimumWidth(window->size().width()/3);
}
