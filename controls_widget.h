#ifndef CONTROLS_WIDGET_H
#define CONTROLS_WIDGET_H

#include <QtWidgets>
#include <QMediaPlayer>
#include "the_player.h"
#include "the_button.h"

class ControlsWidget: public QWidget{
Q_OBJECT
public:
    ControlsWidget(ThePlayer* player);

    void setPlaystate(bool playState);

    bool getPlayState(){return playstate;};

    void setButtons(std::vector<TheButton*> buttons){this->buttons=buttons;}

    // Define the buttons so that they can be used in all functions
    QPushButton* favButton;
    QPushButton* volButton;
    QPushButton* prevButton;
    QPushButton* pauseButton;
    QPushButton* nextButton;
    QTextEdit* tags;
    QLabel* title;
    QSlider* slider;

signals:
    // signals for pause/play button
    void pause();
    void play();
    void sliderChanged(qint64 pos);

private:

    ThePlayer* media;
    std::vector<TheButton*> buttons;

    // Function to add connections to media player
    void addPlayer(QMediaPlayer* media);

    // saves whether player is currently paused or not
    bool playstate;
    bool starstate = false;

    // functions called when the widget is created
    void createWidgets();
    void setupSlots();

    // function to help format buttons
    void setupButton(QPushButton* button);

private slots:
    // To change the play/pause button
    void playStateTrue();
    void playStateChange();
    void changePlayState();
    void changeStarredState();
    void nextVideo();
    void prevVideo();

public slots:
    void jumpTo(TheButtonInfo* button);
//    void changeState();
    void setSlider(qint64 mili);
    void convSlider();
};

#endif // CONTROLS_WIDGET_H
