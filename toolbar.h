#ifndef TOOLBAR_H
#define TOOLBAR_H

#include <QObject>
#include <QWidget>
#include <QToolBar>
#include <QAction>

//main coder: Tymoteusz Mroczkowski

class Toolbar : public QToolBar
{
Q_OBJECT
public:
    Toolbar();
private:
    // pointers to buttons
    QAction *b1;
    QAction *b2;
    QAction *b3;
    QAction *b4; //Star Trek: Nemesis flashbacks
private slots:

signals:
    void addVid();
    void renameVid();
    void lib(); //signals that the button to open libraryWidget was pressed
};

#endif // TOOLBAR_H
