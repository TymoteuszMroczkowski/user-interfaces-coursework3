//
// Created by twak on 11/11/2019.
//

#include "the_button.h"
#include "controls_widget.h"
#include <iostream>

using namespace std;

TheButton::TheButton(QWidget *parent) :  QToolButton(parent) {
    setIconSize(QSize(200,110));
    connect(this, SIGNAL(released()), this, SLOT (clicked() )); // if QPushButton clicked...then run clicked() below
}

void TheButton::init(TheButtonInfo* i) {
    setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    setIcon( *(i->icon) );
    info =  i;
    setText(i->name);
    setStyleSheet("background: white;");
}


void TheButton::clicked() {
    this->info->isClicked = true;
    emit jumpTo(info);
}

void TheButton::setControlWidget(ControlsWidget* controlWidget){
    this->controlWidget=controlWidget;
}

QString TheButtonInfo::computeName(QUrl url_)
{
    QString name = url_.fileName();
    return name;
}

TheButtonInfo::TheButtonInfo ( QUrl* url_, QIcon* icon_)
{
    url=url_;
    icon=icon_;
    name=computeName(*url);
}

void TheButtonInfo::setVectorIndex(int index){
    vectorIndex = index;
}
