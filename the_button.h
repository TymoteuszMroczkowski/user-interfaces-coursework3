//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_BUTTON_H
#define CW2_THE_BUTTON_H
#include <QPushButton>
#include <QUrl>
#include <QLabel>
#include <QToolButton>

class ControlsWidget;

class TheButtonInfo {

public:
    QUrl* url; // video file to play
    QIcon* icon; // icon to display
    QString name;
    TheButtonInfo ( QUrl* url, QIcon* icon);
    QString computeName(QUrl url_);
    int vectorIndex = 0;
    bool isClicked = false;
    void setVectorIndex(int index);
    int getVectorIndex(){return vectorIndex;}
};

class TheButton : public QToolButton {
    Q_OBJECT

public:
    TheButtonInfo* info;
    bool playstate;

    void setVectorIndex(int index){vectorIndex = index;}
    int getVectorIndex(){return vectorIndex;}

    TheButton(QWidget *parent);

    void init(TheButtonInfo* i);

    void setControlWidget(ControlsWidget* controlWidget);

private:
    ControlsWidget* controlWidget;
    int vectorIndex;

public slots:
    void clicked();

signals:
    void jumpTo(TheButtonInfo*);

};

#endif //CW2_THE_BUTTON_H
